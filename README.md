# Enquete Data SNT 
Un jeu d'enquête permettant de découvrir les bases de données relationnelles 
en classe de SNT (et plus)

## Comment utiliser cet outil ?
Ce site est développé en php avec le framework Symfony v4.4 . Pour l'utiliser, il faut 
installer une stack LAMP ou WAMP classique (Apache Mysql Php).

Configuration requise : 
 * Php doit être en version 7.2 ou plus
 * composer doit être installé
 
 Vous trouverez des instructions précises sur le déploiement de Symfony [ici](https://symfony.com/doc/current/deployment.html)
 
 ## Serveur local intégré
 Vous pouvez aussi utiliser le serveur intégré de Symfony en utilisant la commande 
 **symfony server:start**, mais c'est peu conseillé en production. Cependant, si vous
 n'êtes pas confiant dans vos capacités informatiques ou souhaitez juste déployer cet
 outil dans une salle de classe, ce server sera largement suffisant ! 
 
 Plus d'informations sont disponibles [ici](https://symfony.com/doc/4.4/setup/symfony_server.html)
 
## Configuration nécessaire avant installation
 * Vous devez modifier le fichier .env avec les valeurs correspondant à votre environnement 
 de production puis utiliser la commande **composer dump-env prod** pour obtenir un fichier
 .env.local utilisable en production. Ce fichier sera utilisé par défaut une fois qu'il
 sera généré
 
 * Vous devez exécuter la commande **composer install** à la racine du projet
 
 ## Installation
  1. Installez une stack LAMP ou WAMP (ou EasyPhp, UWamp, etc.). 
  Si vous souhaitez n'utiliser que le serveur interne de Symfony,
  vous devrez tout de même installer Mysql
  2. Exécutez le script 'script.sql' sur votre base de données Mysql
  3. Paramétrez l'application comme décrit ci-dessus. Si vous avez utilisé le script.sql,
  les paramètres de base devraient suffir
  4. Lancez le serveur Symfony, puis connectez-vous à l'adresse qu'il vous indique (localhost:8000)
  pour tester le fonctionnement de l'application. Vous pouvez aussi directement déployer
  sur votre serveur web Apache ou Nginx (ou autre)
  
# Licence
Activité d'origine issue du site de [Marie Duflot-Kremer](https://members.loria.fr/MDuflot/) (que je remerci au passage pour cette activité ainsi que ses nombreuses autres activités d'informatique déconnectée)

Ce projet est à mon unique initiative.

Ce projet est sous licence [Creative Commons Attribution - Non Commercial - Share Alike](https://creativecommons.org/licenses/by-nc-sa/4.0/)

 