const contenu = {
    page1: { title: 'Instructions',
        content: "Vous êtes un Data Detective en apprentissage : un pro qui sait utiliser les données pour répondre à des questions, résoudre des enquêtes,\
                     ...Vous diposez d'un Terminal, sur le côté, pour envoyer des commandes vers la base de données. La base de données est décrite en dessous. \
                     <br/><br/>Commence par une requête simple : récupère l'année, la filière et le niveau d'Inscription de l'étudiant dont le numéro est 16. \
                     Pour faire ça, tape la requête suivante dans ton Terminal :<br/><b>SELECTIONNER annee, filiere, niveau<br/>DANS Inscriptions<br/>TELS QUE num_et=16</b><br/>\
                     <ul>\
                       <li><b>SELECTIONNER</b> te permet de choisir les champs/descripteurs que tu veux sélectionner</li><br/> \
                       <li><b>DANS</b> précise la table/collection dans laquelle sont ces descripteurs</li><br/> \
                       <li><b>TELS QUE</b> permet de filtrer les informations en précisant des valeurs qui doivent obligatoirement apparaître. \
                          Ici on n'affiche que les informations de l'étudiant qui a le numéro 16</li>\
                       <li>Dès que tu travailles avec une valeur textuelle dans <b>TELS QUE</b>, il faut que tu l'entoure de guillemets.<br/>\
                            Ex.: <b>TELS QUE prenom='Pierre'</b></li>\
                     </ul><br/>\
                    Entraine toi : trouve la liste des filières de l’université"
    },
    page2: { title: 'Ordonner',
        content: "Tu peux ordonner les données en utilisant <b>TRIER PAR descripteur</b>. \
                    Les données seront ordonnées dans l'ordre croissant des valeurs du descripteur. \
                    Par exemple :<br/><br/><b>SELECTIONNER prenom, nom<br/>DANS Etudiants<br/>TRIER PAR prenom</b><br/><br/>\
                    Attention ! <b>TRIER PAR</b> doit toujours être placé tout à la fin de la requête ! Sinon tu auras une erreur."
    },
    page3: { title: 'De longs livres',
        content: "Trouve la liste des livres (uniquement leur titre) ayant 150 pages ou plus. (Indice : tu peux utiliser les opérateurs <, >, <=, >= et != (pas égal à, différent de))"
    },
    page4: { title: 'Homonymes',
        content: "Trouve la liste d'étudiants (num_et et prenom) ayant le même prénom que vous (il peut ne pas y en avoir)."
    },
    page5: { title: 'Documents en retard',
        content: "Trouve la liste des livres n'ayant pas encore été rendus. Tu peux utiliser attribut <b>EST VIDE</b> dans <b>TEL QUE</b>. <br/><br/>\
                  Exemple : <b>SELECTIONNER nom <br/>\
                  DANS Etudiants <br/>\
                  TELS QUE prenom EST VIDE</b>"
    },
    page6: { title: 'Liste des lecteurs',
        content: "Trouve la liste des numéros des étudiants qui ont encore des livres non-rendus à la bibliothèque."
    },
    page7: { title: 'Je ne suis pas un numéro !',
        content: "Il est souvent utile de pouvoir recouper les données de deux tables. \
                     Par exemple, on veut la liste des prénoms et noms des étudiants ayant encore des livres non-rendus, pas seulement leur numéro. \
                     Il va falloir pour cela dire que le num_et de la table <b>Emprunts</b> est le même que celui de la table <b>Etudiants</b>. On utilise pour cela la jointure : \
                     on écrira par exemple plus <br/><b>SELECTIONNER num_et DANS Inscriptions</b><br/> mais <br/><b>SELECTIONNER Inscriptions.num_et DANS Inscriptions \
                     JOINTURE Etudiants QUAND Inscriptions.num_et = Etudiants.num_et</b><br/> \
                     La jointure couple/relie les informations.<br/><br/>\
                     Attention : Quand tu utilises une jointure, il y a plusieurs descripteurs/champs similaires ! Dans l'exemple au dessus, il y a par exemple deux fois \
                     le descripteur 'num_et' ! Et du coup si tu écris <b>SELECTIONNER num_et DANS Inscriptions...</b> alors la base de données ne saura pas quel num_et choisir ! \
                     Il faut bien préciser celui qu'on veut ! <br/>\
                     On précise en écrivant <b>Nom_Table.descripteur</b>"
    },
    page8: { title: 'Quelques statistiques',
        content: "Donnez la liste des dates d'emprunts des livres de 150 pages ou plus."
    }

}

function fillInstructionsTemplate(pageNumber) {
    const destination = document.getElementById("text-to-change");
    destination.innerHTML = "";
    const fragment = document.getElementById("instructions-template");
    const instance = document.importNode(fragment.content, true);
    const content = contenu["page"+pageNumber];
    instance.querySelector('#page-number').setAttribute("pageNumber", pageNumber);
    instance.querySelector("#instructions-title").innerHTML = content.title;
    instance.querySelector("#instructions-content").innerHTML = content.content;
    destination.appendChild(instance);
    document.querySelector('body').scrollIntoView();
}

function sendRequest() {
    const content = document.querySelector('#input').value;
    document.querySelector("#output").value = "";
    let xhr = new XMLHttpRequest();
    xhr.open('POST', '/send-request');
    xhr.onload = () => {
        if (xhr.status >= 200 && xhr.status < 300) {
            fillOutput(xhr.response);
        } else {
            console.log(xhr.statusText);
            fillOutput("Il y a eu une erreur, veuillez réessayer !")
        }
    };
    xhr.onerror = () => console.log(xhr.statusText);
    xhr.send(content);
}

function fillOutput(value) {
    document.querySelector("#output").value = value;
}

console.log(document.referrer);
if (document.referrer.indexOf("page2") != -1) {
    fillInstructionsTemplate(8);
} else {
    fillInstructionsTemplate(1);
}

function nextQuestion() {
    const pageNumber = parseInt(document.getElementById("page-number").getAttribute("pageNumber"), 10)+1;
    if (pageNumber < 9) {
        fillInstructionsTemplate(pageNumber);
    } else {
        window.location.href = "page2";
    }
}

function previousQuestion() {
    const pageNumber = parseInt(document.getElementById("page-number").getAttribute("pageNumber"), 10)-1;
    if (pageNumber > 0) {
        fillInstructionsTemplate(pageNumber);
    } else {
        window.location.href = "/";
    }
}
