<?php

namespace App\Controller;


use Symfony\Component\Routing\Annotation\Route;

class Page2Controller extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    /**
     * @Route("/page2", name="page2")
     */
    public function index() {
        return $this->render('page2.html.twig');
    }
}