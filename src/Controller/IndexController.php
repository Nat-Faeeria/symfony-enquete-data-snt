<?php

namespace App\Controller;


use Symfony\Component\Routing\Annotation\Route;

class IndexController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index() {
        return $this->render('index.html.twig');
    }
}