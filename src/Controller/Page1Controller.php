<?php

namespace App\Controller;


use Symfony\Component\Routing\Annotation\Route;

class Page1Controller extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    /**
     * @Route("/page1", name="page1")
     */
    public function index() {
        return $this->render('page1.html.twig');
    }
}