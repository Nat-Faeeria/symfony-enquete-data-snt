<?php

namespace App\Controller;

use Doctrine\DBAL\Connection;
use PDO;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\UnicodeString;

class SendRequestController extends AbstractController
{
    /**
     * @Route("/send-request", name="send-request")
     */
    public function sendRequest(Request $request, Connection $connection, LoggerInterface $logger) {

        $returnData = new UnicodeString();
        $fakeSQL = new UnicodeString($request->getContent());
        $realSQL = $this->transformRequestToSQL($fakeSQL);

        $statement = $connection->prepare($realSQL);
        try {
            $statement->execute();
        } catch (DBALException $e) {
            $logger->error("argh");
        }
        $rawData = $statement->fetchAll();
        foreach($rawData as $row) {
            $returnData = $returnData->append(implode(", ",$row)."\n");
        }
        return new Response($returnData, Response::HTTP_OK);
    }

    private function transformRequestToSQL(UnicodeString $request) {
        $request = $request->replace("SELECTIONNER", "SELECT DISTINCT")
            ->replace("Selectionner", "SELECT DISTINCT")
            ->replace("selectionner", "SELECT DISTINCT")
            ->replace("Sélectionner", "SELECT DISTINCT")
            ->replace("sélectionner", "SELECT DISTINCT");

        $request = $request->replace("DANS", "FROM")
            ->replace("Dans", "FROM")
            ->replace("dans", "FROM");

        $request = $request->replace("TELS QUE", "WHERE")
            ->replace("Tels que", "WHERE")
            ->replace("Tels Que", "WHERE")
            ->replace("tels que", "WHERE");

        $request = $request->replace("TRIER PAR", "ORDER BY")
            ->replace("Trier Par", "ORDER BY")
            ->replace("Trier par", "ORDER BY")
            ->replace("trier par", "ORDER BY");

        $request = $request->replace("EST VIDE", "IS NULL")
            ->replace("Est Vide", "IS NULL")
            ->replace("Est vide", "IS NULL")
            ->replace("est vide", "IS NULL");

        $request = $request->replace("JOINTURE", "INNER JOIN")
            ->replace("Jointure", "INNER JOIN")
            ->replace("jointure", "INNER JOIN");

        $request = $request->replace("QUAND", "ON")
            ->replace("Quand", "ON")
            ->replace("quand", "ON");

        $request = $request->replace("ET ", "AND ");

        $request = $request->replace("COMME", "LIKE")
            ->replace("Comme", "LIKE")
            ->replace("comme", "LIKE");
        return $request;
    }
}